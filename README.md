# Buildroot-based Gitlab Runner

This repository provides basic configuration to construct a virtual appliance
that runs a [Gitlab CI runner](https://docs.gitlab.com/ee/ci/) for Docker-based
builds, using [Buildroot](https://buildroot.org/).

## Building

The `external` directory is meant to be used as a buildroot external tree,
specified via the `BR2_EXTERNAL` variable. The `buildroot` submodule in this
repository is pinned to the version of buildroot it has been developed against.

To configure the build, from the root of the repository:

    make -C buildroot \
        BR2_EXTERNAL=$(pwd)/external \
        gitlab_runner_appliance_defconfig

Setting `BR2_EXTERNAL` makes the build system load configs provided in the
`external` directory, and `gitlab_runner_appliance_defconfig` specifies to use
the configuration options specified by this project.

### Configuring the runner

The variable `GITLAB_RUNNER_CONFIG_FILE` must be set to the path to a file
containing the [Gitlab Runner configuration
file](https://docs.gitlab.com/runner/configuration/) that will be build into the
system image. By default it refers to a file `config.toml` in the root of the
repository.

The simplest way to generate a basic configuration is to run `gitlab-runner
register` on a system of your choice:

    gitlab-runner register --config ./config.toml --executor docker

This will prompt for some additional values that are specific to a single runner
(some of which should be kept secret) and write the new `config.toml` file on
completion.

The provided `config.toml.example` file offers a sample of appropriate
configuration for running [Docker-in-Docker
builds](https://docs.gitlab.com/runner/executors/docker.html#use-docker-in-docker-with-privileged-mode);
merging the parts of the configuration specific to your runner with this sample
is a quick and easy way to get started and take advantage of the isolation
offered by a virtual machine like this.

### Building an image

The default configuration builds a kernel image and root filesystem suitable for
running under [Qemu](https://www.qemu.org/), as used by many virtualization
platforms.

    make

When the build is complete, the files `bzImage` and `rootfs.squashfs` will be
written to the `images` directory in the output path (`buildroot/output/` by
default).

## Running

Exact configuration will depend on what hypervisor is used to run the appliance.
This documentation demonstrates manual invocation with `qemu-system-x86_64`,
which can be easily translated to [libvirt](https://libvirt.org/) configuration
with any supported hypervisor or used as a guide for others.

### Persistent storage

The root filesystem is a squashfs, which is read-only at all times. To store
persistent data (container images and the like) the system also requires a data
volume, which is expected to be provided as a filesystem with label
`docker-data`. The only supported filesystem in the default configuration is
ext4 because the system kernel isn't built with support for any other
filesystems.

One way to generate a QCOW2 image file containing such a filesystem, with 20GB
capacity:

    qemu-img create docker.img 20G
    mkfs.ext4 -L docker-data docker.img
    qemu-img convert -O qcow2 docker.img docker.qcow2
    rm docker.img

---

The system runs [docuum](https://github.com/stepchowfun/docuum) to limit the
amount of storage consumed by Docker images, currently hard-coded to 10GB,
so the total amount of storage required for this filesystem should not greatly
exceed that limit plus whatever scratch space is used during builds.

### Networking

The system will auto-configure all network interfaces that use the `virtio`
driver, so simply ensuring the virtual machine has such a device should be
sufficient for most users.

### qemu-system invocation

The hypervisor must be configured to load the kernel directly because the root
filesystem does not contain a boot loader or a copy of the kernel, and the root
filesystem as well as the data volume must be attached as disks.

    qemu-system-x86_64 \
        -kernel bzImage \
        -append "rootwait root=/dev/vda console=ttyS0" \
        -nic user,model=virtio-net-pci \
        -drive file=rootfs.squashfs,if=virtio,format=raw \
        -drive file=docker.qcow2,if=virtio,format=qcow2

If all goes well, this VM should boot up and provide a login prompt on its
**serial console**. This uses the serial console rather than the regular VGA
console because the VGA console didn't work when I tried to use it, but the
serial console in Qemu works okay; removing `console=ttyS0` from the kernel
command line (`-append` option to qemu) might allow the VGA console to be used
on other systems.

The default root password is `test0000`; this can be configured through the
usual buildroot procedure if desired. The system can be managed via the shell,
in particular using `systemctl` to inspect and modify the state of services
including the Gitlab runner, Docker and docuum.

## Development notes

Update defconfig with manual changes (via menuconfig):

    make savedefconfig BR2_DEFCONFIG=$(pwd)/external/configs/gitlab_runner_appliance_defconfig
    
