include $(sort $(wildcard $(BR2_EXTERNAL_GITLAB_RUNNER_APPLIANCE_PATH)/package/*/*.mk))

define GITLAB_RUNNER_INSTALL_CONFIG
	install -D -m 600 $(GITLAB_RUNNER_CONFIG_FILE) $(TARGET_DIR)/etc/gitlab-runner/config.toml
endef