DOCUUM_VERSION = v0.21.1
DOCUUM_SITE = $(call github,stepchowfun,docuum,$(DOCUUM_VERSION))
DOCUUM_LICENSE = MIT
DOCUUM_LICENSE_FILES = LICENSE.md

$(eval $(cargo-package))
